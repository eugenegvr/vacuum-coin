<?php

use Illuminate\Support\Facades\Route;

Route::get('/')->name('faucet.index')->uses('FaucetController@index');
