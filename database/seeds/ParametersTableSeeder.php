<?php

use \App\Parameters;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class ParametersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Parameters::create([
            'name'  => 'faucet_balance_btn',
            'value' => 0,
        ]);
        Log::stack(['errorlog', 'slack'])->info('parameter "faucet_balance_btn" is added');

    }
}
