<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;

class Claims extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'ip', 'payout_id', 'amount'
    ];

    public static function getTimeForNextClaim($value, $valueName)
    {
        $claimParams = config('faucet')['claim'];
        $waitTime = 0;
        $claim = self::select(['created_at'])
            ->where([
                $valueName => $value,
                'referral' => 0
            ])
            ->orderBy('created_at','desc')
            ->first();

        if(!empty($claim)) {
            $claim = $claim->toArray();
            $claimTime = date(
                'Y-m-d H:i:s',
                strtotime(
                    sprintf(
                        '+%s %s',
                        $claimParams['period'],
                        $claimParams['measurement']
                    ),
                    strtotime($claim['created_at'])
                )
            );
            if (strtotime(date('Y-m-d H:i:s')) < strtotime($claimTime)) {
                $waitTime = (strtotime($claimTime) - strtotime(date('Y-m-d H:i:s'))) / 60;
                $waitTime = $waitTime == $claimParams['period'] ? $waitTime : $waitTime + 1;
            }
        }

        return (int)$waitTime;
    }

    public static function writeClaim($address, $ip, $payout_id, $amount, $referral = false)
    {
        $claim = new Claims;
        $claim->address = $address;
        $claim->ip = $ip;
        $claim->payout_id = (int)$payout_id;
        $claim->amount = $amount;
        $claim->referral = $referral;
        $claim->save();
    }

    public static function generateAmount()
    {
        $probabilities = config('faucet')['claim_amount_range']['probabilities'];
        $parsedProbabilities = [];
        $sum = 0;
        foreach ($probabilities as $key => $probability) {
            $sum += $probability['probability'];
            $parsedProbabilities[(string)$sum] = [
                'min' => $probability['min'],
                'max' => $probability['max']
            ];
        }
        $rand = rand(0, 1000);
        $amount = 0;
        foreach ($parsedProbabilities as $key => $parsedProbability) {
            if ($rand  <= ((float)$key) * 10) {
                $amount = rand($parsedProbability['min'], $parsedProbability['max']);
                break;
            }
        }

        return $amount;
    }

    public static function getClaimers($period)
    {
        return self::select([
                'address',
                DB::raw('count(*) as claims'),
                DB::raw('sum(amount) as total'),
            ])
            ->where('created_at', '>=', Carbon::now()->subDays($period)->toDateTimeString())
            ->groupBy('address');
    }

    public static function getDailyLeftClaims($address)
    {
        $claimsDaily = config('faucet')['claims_daily'];
        $claimsCount = 0;
        if (!empty($address)) {
        $claims = self::select(['created_at'])
            ->where(['address' => $address])
            ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('today midnight')))
            ->orderBy('created_at','desc')
            ->get();

        if(!empty($claims)) {
            $claimsCount = $claims->count();
        }
        }


        return (int)$claimsDaily - (int)$claimsCount;
    }
}
