<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    public static function updateUsers() {
        try {
            $period = 5;
            $claimers = Claims::getClaimers($period);
            if (empty($claimers)) {
                throw new \Exception("Claimers for for last " . $period . " not found");
            }
            DB::beginTransaction();
            $newClaims = $claimers
                ->get()
                ->toArray();
            $claimers->delete();
            $new = 0;
            $updated = 0;

            foreach ($newClaims as $newClaim) {
                $user = self::where(['address' => $newClaim['address']])
                    ->first();

                if (!$user) {
                    $user = new Users;
                    $user->address = $newClaim['address'];
                    $user->claims = $newClaim['claims'];
                    $user->total = $newClaim['total'];
                    $new++;
                } else {
                    $user->claims += $newClaim['claims'];
                    $user->total += $newClaim['total'];
                    $updated++;
                }
                $user->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status' => 0,
                'message' => sprintf(
                    'Something went wrong during users table. Message: [%s]',
                    $e->getMessage()
                )
            ];
        }

        return [
            'status' => 1,
            'new' => $new,
            'updated' => $updated
        ];
    }

    public static function getUsersCount() {
        return self::count();
    }
}
