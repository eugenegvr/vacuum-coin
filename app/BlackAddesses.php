<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlackAddesses extends Model
{
    public static function isAddressBanned($address)
    {
        $blackAddress = self::select()
            ->where(['address' => $address])
            ->first();

        return (bool)$blackAddress;
    }
}
