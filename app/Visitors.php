<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitors extends Model
{
    public static function writeVisit($ip)
    {
        $visitor = new Visitors();
        $visitor->ip = $ip;
        $visitor->save();
    }
}
