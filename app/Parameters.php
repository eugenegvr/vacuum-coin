<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Parameters extends Model
{
    public function updateBalance($balance = 0, $currency = 'btn') {
        try {
            $name = 'faucet_balance_'.$currency;
            $parameter = $this
                ->where(['name' => $name])
                ->first();

            if (empty($parameter)) {
                throw new \Exception("Parameter with such name not found");
            }
            DB::beginTransaction();
            $parameter->value = $balance;
            $parameter->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return [
                'status' => 0,
                'message' => sprintf(
                    'Something went wrong during updating "%s" parameter.\n Message: [%s]',
                    $name,
                    $e->getMessage()
                )
            ];
        }

        return [
            'status' => 1
        ];
    }

    static public function getBalance($currency = 'btn') {
        $name = 'faucet_balance_'.$currency;
        $balance = self::select(['value'])
            ->where(['name' => $name])
            ->first()
            ->toArray();
        if (empty($balance)) {
            return false;
        }

        return strlen((string)$balance['value']) > 3 ?
            round($balance['value'] / 1000, 1) . 'k' :
            $balance['value'];
    }
}
