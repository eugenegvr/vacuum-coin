<?php

namespace App\Http\Controllers;

use App\BlackAddesses;
use App\Claims;
use App\Users;
use App\Parameters;
use App\Visitors;
use Illuminate\Http\Request;
use App\Traits\Faucet;
use App\Traits\hCaptcha;
use Illuminate\Support\Facades\URL;

class FaucetController extends Controller
{
    use Faucet, hCaptcha {
        Faucet::getData insteadof hCaptcha;
        Faucet::generateStringFields insteadof hCaptcha;
    }

    public function index()
    {
        $ip = $this->getUserRealIp();
        Visitors::writeVisit($ip);
        $args = $this->getArgument();
        if (!empty($args['r']) && empty($_COOKIE['ref-address'])) {
            setcookie('ref-address', $args['r'], time() + (86400 * 30), "/");
        }
        if (!env('TEST_MODE')) {
            $timeToNextClaim = Claims::getTimeForNextClaim($ip, 'ip');
        } else {
            $timeToNextClaim = 0;
        }

        $redirectInfo = (array)json_decode($_COOKIE['redirect-info'] ?? '');
        unset($_COOKIE['redirect-info']);
        $online = (strtotime(date('Y-m-d')) - strtotime(config('faucet')['start_date'])) / (60 * 60 * 24);
        $referralLink = sprintf('%s/?r=%s',
            config('app')['url'],
            $_COOKIE['address'] ?? 'your_address'

        );
        return view(
            'faucet',
            [
                'showAd' => env('SHOW_ADS'),
                'showRefLink' => env('SHOW_REF_LINK'),
                'timeToNextClaim' => $timeToNextClaim,
                'address' => $_COOKIE['address'] ?? '',
                'referralLink' => $referralLink,
                'balance' => Parameters::getBalance() ?? '0',
                'claimers' => Users::getUsersCount(),
                'online' => $online,
                'period' => config('faucet')['claim']['period'],
                'referralPart' => config('faucet')['referral_part'],
                'measurement' => ucfirst(config('faucet')['claim']['measurement']),
                'claimRange' => config ('faucet')['claim_amount_range'],
                'dailyLeftClaims' => Claims::getDailyLeftClaims($_COOKIE['address'] ?? ''),
                'redirectInfo' => $redirectInfo,
            ]
        );
    }

    public function claim(Request $request)
    {
        try {
            $data = $request->validate([
                'captcha_response' => 'nullable',
                'address' => 'required',
                '_token' => 'required'
            ]);
            $data['ip'] = $this->getUserRealIp();

            if (!BlackAddesses::isAddressBanned($data['address'])) {
                if ($this->checkCaptcha($data['captcha_response'])) {
                    $addressData = $this->checkAddress($data['address']);
                    if ($addressData['status']) {
                        setcookie('address', $data['address'], time() + (86400 * 30), "/");
                        $dailyClaimsLeft = Claims::getDailyLeftClaims($data['address'] ?? '');
                        if ($dailyClaimsLeft !== 0) {
                            //TTNC - Time To Next Claim
                            $TTNCbyAddress = Claims::getTimeForNextClaim($data['address'], 'address');
                            $TTNCbyIp = Claims::getTimeForNextClaim($this->getUserRealIp(), 'ip');
                            if ($TTNCbyAddress == 0 && $TTNCbyIp == 0) {
                                $amount = Claims::generateAmount();
                                $payout_id = $this->sendPayment(
                                    $data['address'],
                                    $amount
                                );
                                if (!empty($payout_id)) {
                                    Claims::writeClaim(
                                        $data['address'],
                                        $data['ip'],
                                        $payout_id,
                                        $amount
                                    );
                                    if (!empty($_COOKIE['ref-address']) && $_COOKIE['ref-address'] != $data['address']) {
                                        $addressRefData = $this->checkAddress($_COOKIE['ref-address']);
                                        if ($addressRefData['status']) {
                                            $refAmount = (int)($amount * config('faucet')['referral_part']/100);
                                            $ref_payout_id = $this->sendPayment(
                                                $_COOKIE['ref-address'],
                                                $refAmount,
                                                true
                                            );
                                            if (!empty($ref_payout_id)) {
                                                Claims::writeClaim(
                                                    $_COOKIE['ref-address'],
                                                    $data['ip'],
                                                    $ref_payout_id,
                                                    $refAmount,
                                                    true
                                                );
                                            }
                                        }
                                    }
                                    $response = [
                                        'status' => 1,
                                        'amount'=> $amount,
                                    ];
                                } else {
                                    throw new \Exception('Something went wrong, please try again');
                                }
                            } else {
                                throw new \Exception('You claimed recently, please wait a bit');
                            }
                        } else {
                            throw new \Exception('You reached the limit of you daily claims');
                        }

                    } else {
                        throw new \Exception($addressData['message']);
                    }
                } else {
                    throw new \Exception('Captcha was not resolved correctly');
                }
            } else {
                throw new \Exception('Sorry, you were banned');
            }
        } catch (\Exception $e) {
            $response = [
                'status' => 0,
                'message' => $e->getMessage()
            ];
        }
        setcookie('redirect-info', json_encode($response, true), time() + 5, "/");

        return $response;
    }

    public function getArgument()
    {
        $argumentsStr = parse_url(URL::full(), PHP_URL_QUERY);
        $arguments = explode(',', $argumentsStr);
        $result = [];

        foreach ($arguments as $argument) {
            $argParts = explode('=', $argument);
            if (count($argParts) == 2) {
                $result[$argParts[0]] = $argParts[1];
            }
        }

        return $result;
    }


}
