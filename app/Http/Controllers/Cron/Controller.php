<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    function index($item, $action) {
        Artisan::queue(sprintf('%s:%s',$item, $action));
    }
}
