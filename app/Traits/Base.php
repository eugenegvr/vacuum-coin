<?php

namespace App\Traits;

trait Base
{
    function getData($url, $data = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_STDERR, fopen('php://stderr', 'w'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 600);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->generateStringFields($data));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;

    }

    function generateStringFields($fields)
    {
        foreach ($fields as $name => $value) {
            $fields[$name] = sprintf('%s=%s', $name, $value);
        }

        return implode('&', $fields);
    }
 }
