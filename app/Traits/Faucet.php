<?php

namespace App\Traits;

trait Faucet
{
    use Base;

    function getBalance() {
        $resp = json_decode($this->getData(
            config('faucet')['api_url'] . 'balance',
            [
                "api_key" => config('faucet')['api_key']
            ]
        ), true);

        return $resp;
    }

    function checkAddress($address) {
        $resp = json_decode($this->getData(
            config('faucet')['api_url'] . 'checkaddress',
            [
                "api_key" => config('faucet')['api_key'],
                "address" => $address,
            ]
        ), true);

        if ( $resp['status'] === 200) {
            $result = [
                'status' => 1,
            ];
        } else {
            $result = [
                'status' => 0,
                'message' => $resp['message']
            ];
        }
        return $result;
    }

    function sendPayment($address, $amount, $referral = false) {
        $resp = json_decode($this->getData(
            config('faucet')['api_url'] . 'send',
            [
                "api_key" => config('faucet')['api_key'],
                "amount" => $amount,
                "to" => $address,
                "referral" => (int)$referral,
            ]
        ), true);

        return ($resp['status'] == 200) ? $resp['payout_id'] : '';
    }
 }
