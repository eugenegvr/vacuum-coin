<?php

namespace App\Traits;

trait hCaptcha
{
    use Base;

    function checkCaptcha($token) {

        $resp = json_decode($this->getData(
            'https://hcaptcha.com/siteverify',
            [
                'secret' => '0x90aF2C0366dD022d33E42011a1Af91297c09E906',
                'response' => $token,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ]
        ), true);

        return (!empty($resp['success']) && $resp['success'] == 1) ? 1 : 0;
    }
 }
