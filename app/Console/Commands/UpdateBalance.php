<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\Faucet;
use Illuminate\Support\Facades\Log;
use \Carbon\Carbon;
use App\Parameters;

class UpdateBalance extends Command
{
    use Faucet;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get balance from faucet
        $apiResponse = $this->getBalance();

        if (isset($apiResponse['status']) && $apiResponse['status'] == 200) {
            $message = sprintf(
                'balance request completed, balance - %s',
                $apiResponse['balance']

            );
        } else {
            $message = sprintf(
                'balance request failed, reason - %s',
                $apiResponse['message']

            );
        }
        Log::info($message);
        Log::stack(['errorlog', 'slack'])->info($message);

        //update balance in the DB
        $parameter = new Parameters();
        $dbResponse = $parameter->updateBalance($apiResponse['balance']);

        $message = (isset($dbResponse['status']) && $dbResponse['status'] == 1) ?
            'balance update completed' :
            sprintf(
                'balance update failed: %s',
                $dbResponse['message']
            );

        Log::info($message);
        Log::stack(['errorlog', 'slack'])->info($message);
    }
}
