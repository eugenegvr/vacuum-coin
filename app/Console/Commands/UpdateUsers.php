<?php

namespace App\Console\Commands;

use App\Users;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class UpdateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //update users in the DB
        $dbResponse = Users::updateUsers();

        if($dbResponse['status']){
            $message = sprintf(
                'users update completed. new users - %s, updated users - %s',
                $dbResponse['new'],
                $dbResponse['updated']
            );
        }else{
            $message =  sprintf(
                'users update failed: %s',
                $dbResponse['message']
            );
        }

        Log::info($message);
        Log::stack(['errorlog', 'slack'])->info($message);
    }
}
