<?php

return [
    'api_key' => 'd8eedb3ebf116c1a8c4b1018aeee715c759771b0',
    'api_url' => 'https://faucetpay.io/api/v1/',
    'start_date' => '2020-05-06',
    'claim' => [
        'period'        => 2,
        'measurement'   => 'hours'
    ],
    'claims_daily'  => 10,
    'referral_part' => 20, //in percent(%)
    'claim_amount_range' => [
        'min' => 20,
        'max' => 100,
        'probabilities' => [
            // set probability in increasing order
            [
                'min' => 20,
                'max' => 22,
                'probability' => 95
            ],
            [
                'min' => 23,
                'max' => 26,
                'probability' => 4
            ],
            [
                'min' => 27,
                'max' => 50,
                'probability' => 0.9
            ],
            [
                'min' => 51,
                'max' => 100,
                'probability' => 0.1
            ],
        ]
    ],
];
