<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}">

        <title>Vacuum-coin</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Raleway&family=Questrial" rel="stylesheet">
        <script src="https://hcaptcha.com/1/api.js" async defer></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #f1f5f8;
                color: #636b6f;
                margin: 0;
            }
            body {
                background: #f1f5f8;
                display: flex;
            }
            .left-field, .right-field {
                width: calc((100% - 730px) / 2);
            }
            .central-field {
                width: 730px;
                min-width: 700px;
            }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .shadow-inner {
                box-shadow: inset 0 2px 4px 0 rgba(0, 0, 0, .06);
            }
            .rounded {
                border-radius: .25rem;
            }

            .shadow {
                box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .1);
            }
            .content {
                text-align: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .info {
                display: flex;
                justify-content: space-between;
            }
            .info > * {
                background: #fff;

                width: calc(100% / 3);
                margin: 5px;
                padding: 15px 10px;
            }
            .info-title {
                font-size: 18px;
                line-height: 20px;
                font-family: 'Raleway', sans-serif;
                text-align: center;
            }
            .info-value {
                font-family: 'Questrial', sans-serif;
                text-align: center;
                font-size: 1.4em;
            }
            .title {
                width: 520px;
                margin: 30px calc((100% - 520px)/2);
                font-family: 'Raleway', sans-serif;
                display: flex;
            }

            .name {
                font-weight: 900;
                font-size: 80px;
                line-height: 100px;
            }

            .domain {
                font-weight: 400;
                font-size: 40px;
                padding-top: 10px;
                transform: rotate(-90deg);
            }

            .description {
                font-family: 'Questrial', sans-serif;
                background: #fff;
                font-size: 18px;
                line-height: 20px;
                padding: 10px;
                margin: 5px;
            }

            .address {
                font-family: 'Releway', sans-serif;
                width: calc(100% - 100px);
                margin: 50px 50px 10px 50px;
                text-align: center;
                color: #636b6f;
                padding: 5px 10px;
                border-radius: 3px;
                border: 1px solid #CCC;
            }

            .ref-container {
                margin: 50px 5px 5px 5px;
                background: #fff;
                padding: 10px;
            }

            .ref-container p {
                font-family: 'Questrial', sans-serif;
                padding: 40px 50px 5px 50px;
                font-size: 18px;
                line-height: 20px;
                text-align: center;
                color: #007bff;
            }

            .ref-address {
                font-family: 'Releway', sans-serif;
                width: calc(100% - 100px);
                margin: 5px 50px 40px 50px;
                text-align: center;
                color: #636b6f;
                padding: 5px 10px;
                border-radius: 3px;
                border: 1px solid #CCC;
                font-size: 16px;

            }

            .claim-container {
                background: #fff;
                margin: 5px;
            }

            .btn-container {
                display: flex;
                justify-content: center;
            }

            .claim {
                width: calc(100% - 100px);
                height: 80px;
                margin: 10px 50px 50px 50px;
                text-align: center;
                border: none;
                animation: shake-button 4s infinite;
            }

            .claim:hover {
                animation: none;
            }

            .claim-wait {
                font-family: 'Questrial', sans-serif;
                font-size: 1.1em;
                color: #636b6f;
                width: calc(100% - 100px);
                height: 80px;
                line-height: 80px;
                text-align: center;
                vertical-align: middle;
                margin: 10px 50px 50px 50px;
                border: 1px dashed #ccc;
            }

            .modal-title{
                width: 100%;
                font-family: 'Questrial', sans-serif;
                text-align: center;
            }
            .submit {
                width: 300px;
            }

            @keyframes shake-button {
                0% { transform: rotate(0deg); }
                2% { transform: rotate(-2deg); }
                4% { transform: rotate(2deg); }
                6% { transform: rotate(0deg); }
                100% { transform: rotate(0deg); }
            }

            @media (max-width: 720px) {
                body {
                    display: block!important;
                }

                .left-field, .right-field, .central-field {
                    width: 100%;
                    min-width: auto;
                }

                .title {
                    width: 320px;
                    margin: 30px calc((100% - 320px)/2);
                }
                .name {
                    font-size: 50px;
                    line-height: 60px;
                }

                .domain {
                    font-size: 25px;
                    padding-top: 0;
                }
                .description {
                    font-size: 13px;
                    line-height: 15px;
                }
                .claim-wait, .claim {
                    width: calc(100% - 40px);
                    margin: 10px 20px 20px 20px;
                }
                .claim-wait {
                    font-size: 1.2em;
                }
                .info-title {
                    font-size: .9em;
                }
                .info-value {
                    font-size: 1.2em;
                }
                .address {
                    width: calc(100% - 40px);
                    margin: 20px 20px 10px 20px;
                    font-size: .8em;
                }
            }
        </style>
    </head>
    <body>
        <div class="left-field flex-center position-ref">
            @if($showAd)
                <div id="adm-container-9979"></div><script data-cfasync="false" async type="text/javascript" src="//moonads.net/display/items.php?9979&1959&160&600&4&0&2"></script>
            @endif
        </div>
        <div class="central-field position-ref full-height">
            <div class="info">
                <div class="balance rounded shadow">
                    <div class="info-title">
                        balance
                    </div>
                    <div class="info-value">
                        {{$balance}}
                    </div>
                </div>
                <div class="claimers rounded shadow">
                    <div class="info-title">
                        claimers
                    </div>
                    <div class="info-value">
                        {{$claimers}}
                    </div>
                </div>
                <div class="rounded shadow">
                    <div class="info-title">
                        days online
                    </div>
                    <div class="info-value">
                        {{$online}}
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="title">
                    <div class="name">VacuumCoin</div>
                    <div class="domain">.xyz</div>
                </div>
                @if($showAd)
                    <iframe data-aa="1401619" src="//ad.a-ads.com/1401619?size=336x280&background_color=f1f5f8" scrolling="no" style="width:336px; height:280px; border:0px; padding:0; overflow:hidden" allowtransparency="true"></iframe>
                @endif
                <div class="description rounded shadow">
                    Sign up on
                    <a target="_blank" href="https://faucetpay.io">
                        FaucetPay.io.
                    </a>
                    before using this faucet
                </div>
                <div class="description rounded shadow">
                    Claim from {{$claimRange['min']}} to {{$claimRange['max']}} Free Bitcoin Satoshi every {{$period}} {{$measurement}}
                </div>
                <div class="description rounded shadow">
                    You have
                    <b>{{ $dailyLeftClaims }}</b>
                     daily claims left
                </div>
                @if($showAd)
                    <div id="adm-container-9965"></div><script data-cfasync="false" async type="text/javascript" src="//moonads.net/display/items.php?9965&1959&728&90&4&0&1"></script>
                @endif
                <div class="claim-container rounded shadow">
                    <input type="text" placeholder="Enter Your Bitcoin Address (FaucetPay)" class="address" value="{{ $address }}">
                    <div class="btn-container">
                        @if (count($redirectInfo) > 0)
                                @if ($redirectInfo['status'] === 1)
                                    <div class="claim-wait rounded">
                                        {{ $redirectInfo['amount'] }}
                                        satoshi was sent to you on
                                        <a target="_blank" href="https://faucetpay.io">
                                            FaucetPay.io.
                                        </a>
                                    </div>
                                @elseif ($redirectInfo['status'] === 0)
                                    <div class="claim-wait rounded">
                                        {{ $redirectInfo['message'] }}
                                    </div>
                                @endif
                        @elseif ($dailyLeftClaims === 0)
                                <div class="claim-wait rounded">
                                    You reached the limit of you daily claims
                                </div>
                        @elseif ($timeToNextClaim === 0)
                                <button type="button" class="btn btn-primary claim rounded shadow" data-toggle="modal" data-target="#exampleModalCenter">
                                    Claim
                                </button>
                        @else
                            <div class="claim-wait rounded">
                                Wait {{ $timeToNextClaim }} minutes
                            </div>
                        @endif
                    </div>
                </div>
                @if($showRefLink)
                    <div class="ref-container rounded shadow">
                        <p>Earn {{$referralPart}}% referral commission</p>
                        <input type="text" class="ref-address" value="{{ $referralLink }}">
                    </div>
                @endif
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog  modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Solve captcha and antibot-links to get bitcoin reward</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- Add hCaptcha CAPTCHA box -->
                                <div class="h-captcha" data-sitekey="9fc01766-e814-4f77-995f-299b45a6f6cc"></div>
                                <!-- Submit button -->
                                <button type="button" class="btn btn-primary submit" data-toggle="modal">
                                    Claim
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-field flex-center position-ref">
            @if($showAd)
                <div id="adm-container-9980"></div><script data-cfasync="false" async type="text/javascript" src="//moonads.net/display/items.php?9980&1959&160&600&4&0&2"></script>
            @endif
        </div>
    </body>
    <script>
        $(document).off('click', '.submit').on('click', '.submit', function (e) {
            const token = $("#token").attr('content');
            const captcha_response = $(".h-captcha iframe").data('hcaptcha-response');
            const address = $(".address").val();

            const result = $.post(
                '/api/claim',
                {
                    captcha_response: captcha_response,
                    address: address,
                    _token: token,
                }
            );
            setTimeout(function() {
                window.location.href = window.location.href;
            }, 2000);
        });
    </script>
</html>
